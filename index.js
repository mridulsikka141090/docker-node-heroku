import express from "express";
import bodyParser from "body-parser";
import { v4 as uuidv4 } from "uuid";
const app = express();
const port = process.env.PORT || 3000;
//body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const randomData = {
  id: uuidv4(),
  currentDate: new Date(Date.now()).toISOString(),
  message: "we are running a random test",
  randomNumber: Math.random(100),
};

app.get("/", (req, res) => {
  res.json(randomData);
});

app.listen(port, () => {
  console.log(`Example app listening on ${port}`);
});
